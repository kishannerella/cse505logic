from pulp import *
from random import *
from copy import *
import time

num_rules = 10
max_clauses = 5

logfile = "output.log"
log = 0

def printl(val):
    log.write(str(val))
    log.flush()
    
"""
   Generates a random ap-program
"""
def gen_ap(num_var):
    rules = []
    
    for i in range(num_rules):
        num_clauses = randint(1, max_clauses)
        current_rule = []

        for j in range(num_clauses):
            var = randint(0, num_var-1)
            if var not in current_rule:
                current_rule.append(var)
               
        pval1 = random()
        pval2 = random()
        
        if (pval1 < pval2):
            p_low  = pval1
            p_high = pval2
        else:
            p_low  = pval2
            p_high = pval1
            
        rules.append((current_rule,(p_low, p_high)))
        
    """for rule in rules:
        printl(rule)
    """   
    
    #return [([0,1], (0.4,0.6))]
    return rules
    
    
"""
   Checks whether the world satisfies the variables in the head
"""
def valid_world(vars, world):
    for var in vars:
        if ((1 << var) & world) == 0:
            return False
            
    return True

"""
    Convert ap-program into linear program
    This function returns all the variables and the constraints
"""
def convert_ap_to_lp(num_var, rules):
    pvar = []
    constraints = []
    
    # Create one variable per world
    for i in range(pow(2,num_var)):
        var_name = "p" + str(i)
        pvar.append(LpVariable(var_name, 0, 1))
        
    # Create 2 lp-rules per ap-rules
    for rule, (p_low, p_high) in rules:
        cur_constraint = 0
        for i in range(pow(2, num_var)):
            if valid_world(rule, i):
                cur_constraint += pvar[i]
                
        gcon = cur_constraint >= p_low
        lcon = cur_constraint <= p_high
        constraints.append(gcon)
        constraints.append(lcon)
        
    # Sum of all the probabilities is 1
    sum_constraint = 0
    for i in range(pow(2,num_var)):
        sum_constraint += pvar[i]
    sumcon = sum_constraint == 1    
    
    constraints.append(sumcon)
    """printl(pvar)
    for c in constraints:
        printl(c)"""
    return (pvar, constraints)
    
    
"""
   This the naive algorithm implementation. It takes as input
   the linear program and calculates MPW for the program
"""
def naive(num_vars, lp_program):
    
    pvar, constraints = lp_program
    best_val = 0
    best_world = -1
 
    # Iterate over all the possible worlds 
    for i in range(pow(2,num_vars)):
        #print "World no - " + str(i)
        #if (i % pow(2, num_vars-2) == 0):
        #    printl("World {0} {1}\n".format(i, time.ctime()))
        problem = LpProblem("Naive Max Probable World", LpMinimize)
        
        #Add objective function
        problem += pvar[i]
        
        #Add all constraints
        for constraint in constraints:
            problem += constraint
            
        #print problem
 
        #Solve the linear program
        GLPK().solve(problem)
        
        low_w = value(problem.objective) 
        if (low_w > best_val):
            best_val = low_w
            best_world = i
            
    #printl(best_world)
    
if __name__ == "__main__":

    #Parse arguments
    if len(sys.argv) != 11:
        print("Usage: python mpw.py -s <start_num_var> -e <end_num_var> -r <rules_num_head> -n <num_runs> -l <log_file>")
        print("Eg : python mpw.py -s 5 -e 12 -r 10 -n 10 -l output.log")
        exit()
        
    args = {}
    for i in range(1, 11, 2):
        args[sys.argv[i]] = sys.argv[i+1]
    
    if "-s" in args:
        start = int(args["-s"])
    else:
        exit()
    
    if "-e" in args:
        end = int(args["-e"])
    else:
        exit()
    
    if "-r" in args:
        num_rules = int(args["-r"])
    else:
        exit()
    
    if "-n" in args:
        runs = int(args["-n"])
    else:
        exit()
    
    if "-l" in args:
        logfile = args["-l"]
    else:
        exit()
    
    
    log = open(logfile, "w")
    
    for num_vars in range(start, end+1):
        start_time = time.time()
        for i in range(runs):
            #printl("Var - {0}, Run no - {1}\n".format(num_vars, i))
            rules = gen_ap(num_vars)
            lp_program = convert_ap_to_lp(num_vars, rules)
            pvar, constraints = lp_program
            naive(num_vars, lp_program)
            
        time_taken = (time.time() - start_time)/runs
        printl("{0} {1}\n".format(num_vars, int(time_taken)))

    log.close()